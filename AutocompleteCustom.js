export class AutocompleteCustom {


  /**
   *
   * @param elementInput  Un input de type text
   * @param cssClassContainer  nom de classe css
   */
  constructor(elementInput, cssClassContainer, maxHeight) {
    this.elementInput = elementInput;
    this.cssClassContainer = cssClassContainer;
    this.cssClassListItems = 'ls-opt-items';
    this.cssClassItemActive = 'ls-opt-active';
    this.currentFocus = -1;
    this.maxHeight = maxHeight;
    this.init();
  }

  init() {
    const self = this;
    
    const div = document .createElement('div');
    div.classList.add('ls-opt-container');
    const parent = this.elementInput.parentNode;
    parent.insertBefore(div, this.elementInput);
    div.appendChild(this.elementInput);

    document.addEventListener('click', function (e) {
      self.closeAllLists(e.target);
    });
  }

  addEventKeyUp(callableOnKeyUp) {
    let timer = null;
    const duration = 300;
    this.elementInput.addEventListener('keyup', function(e){
      if (e.key === 'ArrowDown' || e.key === 'ArrowUp') {
        return false;
      }
      if (e.key === 'Enter' || e.key === 'Tab') {
        return false;
      }

      clearTimeout(timer);
      const input = this;
      timer = setTimeout(async function () {
        callableOnKeyUp(input);
      }, duration);
    });
  }

  scrollIntoViewIfNeeded(item) {
    const optionsList = document.getElementById(item.parentElement.id);
    const itemRect = item.getBoundingClientRect();
    const containerRect = optionsList.getBoundingClientRect();

    if (itemRect.bottom > containerRect.bottom) {
      optionsList.scrollTop += itemRect.bottom - containerRect.bottom;
    } else if (itemRect.top < containerRect.top) {
      optionsList.scrollTop -= containerRect.top - itemRect.top;
    }
  }

  addActive(items) {
    if (!items) return false;
    this.removeActive(items);
    if (this.currentFocus >= items.length) this.currentFocus = 0;
    if (this.currentFocus < 0) this.currentFocus = (items.length - 1);
    items[this.currentFocus].classList.add(this.cssClassItemActive);
    this.scrollIntoViewIfNeeded(items[this.currentFocus]);
  }

  removeActive(items) {
    for (let i = 0; i < items.length; i++) {
      items[i].classList.remove(this.cssClassItemActive);
    }
  }

  closeAllLists() {
    const x = document.getElementsByClassName(this.cssClassListItems);
    for (let i = 0; i < x.length; i++) {
      x[i].parentNode.removeChild(x[i]);
    }
  }

  handleKeyDown(event) {
    let items = document.getElementById(this.elementInput.id + 'ls-opt-list');
    if (items) items = items.getElementsByTagName('div');
    switch (event.key) {
      case 'ArrowDown' :
        if (this.currentFocus <= items.length) {
          this.currentFocus++;
        }
        // else {
        //   this.currentFocus = 0;
        // }
        this.addActive(items);
        break;
      case 'ArrowUp' :
        if (this.currentFocus >= -1) {
          this.currentFocus--;
        }
        // else {
        //   this.currentFocus = items.length;
        // }
        this.addActive(items);
        break;
      case 'Enter' :
        event.preventDefault();
        if (this.currentFocus > -1) {
          if (items) items[this.currentFocus].click();
        }
        break;
      case 'Tab' :
        if (this.currentFocus > -1) {
          if (items) items[this.currentFocus].click();
        }
        break;
    }
    return true;
  }

  /**
   *
   * @param arr un array d'objets JSON ayant une clé value qui servira de texte affiché dans la div option
   * @param callableOnClick une fonction appelée en cliquant sur l'option choisie
   * et prenant en paramètres une instance de AutocompleteCustom et l'élement div cliqué (id="optionX")
   */
  populate(arr, callableOnClick) {
    const listItems = this.createList();
    this.fillInList(listItems, arr, callableOnClick);

    this.elementInput.removeEventListener('keydown', this.handleKeyDown.bind(this));
    this.elementInput.addEventListener('keydown', this.handleKeyDown.bind(this));
  }

  createList() {
    this.closeAllLists();
    this.currentFocus = -1;
    const listItems = document.createElement('ul');
    listItems.setAttribute('id', this.elementInput.id + 'ls-opt-list');
    listItems.setAttribute('class', this.cssClassListItems);
    listItems.setAttribute('style', 'maxHeight='+this.maxHeight+'; width='+this.elementInput.offsetWidth.toString()+'px');
    this.elementInput.parentNode.appendChild(listItems);
    return listItems;
  }

  fillInList(listItems, arr, callableOnClick) {
    const self = this;
    for (let i = 0; i < arr.length; i++) {
      const item = document.createElement('li');
      item.innerHTML = arr[i].value;
      item.setAttribute('id', 'option' + i);
      item.setAttribute('data-json', JSON.stringify(arr[i]));
      item.addEventListener('click', () => callableOnClick(self, item));
      item.addEventListener('click', function () {
        self.closeAllLists();
      });
      listItems.appendChild(item);
    }
  }

  getPropertyValue(elementOption, propertyName) {
    const data = JSON.parse(elementOption.getAttribute('data-json'));
    return data ? data[propertyName] : '';
  }

}
