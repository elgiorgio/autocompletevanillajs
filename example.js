import {AutocompleteCustom} from "./AutocompleteCustom";
// import('./AutocompleteCustom.js')

const ville = document.getElementById('ville');
console.debug(ville);
ville.parentElement.classList.add('ls-opt-container');
const autocomplete = new AutocompleteCustom(ville, 'ls-opt-container');
autocomplete.addEventKeyUp(callableOnKeyUp);

async function callableOnKeyUp(input) {
    const urlModel = 'https://geo.api.gouv.fr/communes?££search&fields=code%2Cnom%2CcodesPostaux';

    const url = urlModel.replace('££search', 'nom='.concat(input.value));
    const response = await fetch(url);
    let localites = await response.json();
    const cpVilles = [];
    localites.map(function (localite) {
        for (let i = 0; i < localite.codesPostaux.length; i++) {
            cpVilles.push({
                'id': i + '-' + localite.codesPostaux[i],
                'value': localite.nom.toUpperCase() + ' (' + localite.codesPostaux[i] + ')',
                'label': localite.nom.toUpperCase() + ' (' + localite.codesPostaux[i] + ')',
                'ville': localite.nom.toUpperCase(),
                'cp': localite.codesPostaux[i]
            });
        }
    }, cpVilles);

    if (input.value) {
        autocomplete.populate(cpVilles, callableOnClick);
    }
}

function callableOnClick(autocomplete, elementOption) {
    document.getElementById('ville').value = autocomplete.getPropertyValue(elementOption, 'ville', '280px');
}



/**/
